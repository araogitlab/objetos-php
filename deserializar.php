<?php
    include("student.php");
    
    $s1 = file_get_contents('archivo1');
    $miestudiante = unserialize($s1);
    echo "<br> NOTAS DE UN ESTUDIANTE <br>";
    $miestudiante->imprimir_notas();

    $s2 = file_get_contents('archivo2');
    $estudiantes = unserialize($s2);

    echo "<br> NOTAS DE LOS ESTUDIANTES <br>";
    
    foreach ($estudiantes as $est) {
        $est->imprimir_notas();
        echo "<br>";
    }

    $s3 = file_get_contents('archivo3');
    $estudiantes2 = unserialize($s3);

    echo "<br> NOTAS DE LOS ESTUDIANTES (ArrayObject) <br>";
    
    foreach ($estudiantes2 as $estu) {
        $estu->imprimir_notas();
        echo "<br>";
    }
    
?>
