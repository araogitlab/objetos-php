<?php
class Student{
    public $nombre; 
    public $notas = array(); 
    private $promedio; 

    public function __construct($arg_nombre="",$arg_notas=array()) 
    {
        $this->nombre=$arg_nombre; 
        $this->notas=$arg_notas;
    }
    public function promedio() 
    {
        $total=0; 
        foreach ($this->notas as $nota) 
        {
            $total+=$nota; 
        }
        $this->promedio=$total/4; 
        return $this->promedio; 
    }
    public function imprimir_notas() 
    {
        $i=0;
        foreach ($this->notas as $nota)
        {
            $i++;
            echo "Nota $i es: $nota <br />"; 
        }
    }
	
	function __destruct() {
       print "<br>Destruyendo \n";
   }
}
?>